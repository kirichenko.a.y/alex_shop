// Function to set the active menu item based on the current URL
function setActiveMenuItem() {
  // Get the current page URL
  var currentUrl = window.location.href;

  // Get all menu items
  var menuItems = document.querySelectorAll('.navbar-nav .nav-item .nav-link');

  // Iterate through all menu items
  menuItems.forEach(function(menuItem) {
    // Check if the current URL contains the href attribute of the menu element
    if (currentUrl.includes(menuItem.getAttribute('href'))) {
      // If yes, then set the 'active' class for this element
      menuItem.classList.add('active');
    }
  });
}

// Call a function to set the active menu item when the page loads
document.addEventListener('DOMContentLoaded', setActiveMenuItem);

function includeHTML() {
  var z, i, elmnt, file, xhttp;
  /* Loop through a collection of all HTML elements: */
  z = document.getElementsByTagName("*");
  for (i = 0; i < z.length; i++) {
    elmnt = z[i];
    /*search for elements with a certain atrribute:*/
    file = elmnt.getAttribute("w3-include-html");
    if (file) {
      /* Make an HTTP request using the attribute value as the file name: */
      xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
          if (this.status == 200) {elmnt.innerHTML = this.responseText;}
          if (this.status == 404) {elmnt.innerHTML = "Page not found.";}
          /* Remove the attribute, and call this function once more: */
          elmnt.removeAttribute("w3-include-html");
          includeHTML();
        }
      }
      xhttp.open("GET", file, true);
      xhttp.send();
      /* Exit the function: */
      return;
    }
  }
}